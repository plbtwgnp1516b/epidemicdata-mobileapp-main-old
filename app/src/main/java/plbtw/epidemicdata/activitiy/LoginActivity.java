package plbtw.epidemicdata.activitiy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import plbtw.epidemicdata.R;
import plbtw.epidemicdata.api.ApiUsers;
import plbtw.epidemicdata.api.ServiceGenerator;
import plbtw.epidemicdata.model.LoginBody;
import plbtw.epidemicdata.model.LoginResponse;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText loginEmail;
    EditText loginPassword;
    LinearLayout loginButton;
    TextView signupButton;

    //init validator
    AwesomeValidation mAwesomeValidation;
    ApiUsers apiUsers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginEmail = (EditText) findViewById(R.id.login_email);
        loginPassword = (EditText) findViewById(R.id.login_password);
        loginButton = (LinearLayout) findViewById(R.id.login_button);
        signupButton = (TextView) findViewById(R.id.signUp_button);

        //validator
        mAwesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        mAwesomeValidation.addValidation(this, R.id.login_email, android.util.Patterns.EMAIL_ADDRESS, R.string.app_name);

        apiUsers = ServiceGenerator.createService(ApiUsers.class);

        loginButton.setOnClickListener(this);
        signupButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v == loginButton)
        {
            //check validate
            mAwesomeValidation.validate();
            final ProgressDialog dialog = new ProgressDialog(v.getContext());
            dialog.setMessage("Please wait...");
            dialog.show();
            Call<LoginResponse> call = null;
            LoginBody loginBody = new LoginBody();

            call = apiUsers.cekLogin(loginEmail.getText().toString(),loginPassword.getText().toString());
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                    if (200 == response.code()) {

                        LoginResponse loginResponse = response.body();
                        if (loginResponse.getListDisease().size() != 0) {
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(LoginActivity.this, "salah user", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Error post user", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Failed post user", Toast.LENGTH_SHORT).show();
                }

            });
        }
        else if(v == signupButton)
        {
            Intent i = new Intent(LoginActivity.this, plbtw.epidemicdata.activities.SignupActivity.class);
            startActivity(i);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


}
