package plbtw.epidemicdata.activitiy;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import plbtw.epidemicdata.R;
import plbtw.epidemicdata.api.ApiRedeem;
import plbtw.epidemicdata.api.ServiceGenerator;
import plbtw.epidemicdata.model.RedeemBody;
import plbtw.epidemicdata.model.RedeemPostResponse;
import plbtw.epidemicdata.model.RewardsModel;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailRewardsActivity extends AppCompatActivity {


    @Bind(R.id.imageRewards)
    ImageView imageRewards;

    @Bind(R.id.textNamaRewards)
    TextView textNamaRewards;

    @Bind(R.id.textDeskripsiRewards)
    TextView textDeskripsiRewards;

    @Bind(R.id.textPoin)
    TextView textPoin;

    @Bind(R.id.textSisa)
    TextView textSisa;

    @Bind(R.id.buttonRedeem)
    Button buttonRedeem;


    @OnClick(R.id.buttonRedeem)
    void setOnClickRedeemButton(final View view) {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
        alertDialog.setMessage(Html.fromHtml("<strong>Pertanyan<br/><br/>Apakah Anda yakin?</strong>"));
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {

                final ProgressDialog dialog = new ProgressDialog(view.getContext());
                dialog.setMessage("Please wait...");
                dialog.show();
                Call<RedeemPostResponse> call = null;
                RedeemBody redeemBody = new RedeemBody();
                redeemBody.setId_user_app("2");
                redeemBody.setId_rewards(rewardsModel.getId_rewards());

                redeemBody.setRedeem_key(new Random().nextInt() + "");

                call = apiRedeem.postRedeem(redeemBody);
                call.enqueue(new Callback<RedeemPostResponse>() {
                    @Override
                    public void onResponse(Response<RedeemPostResponse> response, Retrofit retrofit) {
                        dialog.dismiss();
                        if (200 == response.code()) {

                            finish();

                        } else {
                            Toast.makeText(DetailRewardsActivity.this, "Error post redeem", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dialog.dismiss();
                        Toast.makeText(DetailRewardsActivity.this, "Failed post redeem "+t.getMessage(), Toast.LENGTH_LONG).show();
                        textNamaRewards.setText(t.getMessage());
                        textDeskripsiRewards.setText(t.toString());
                        textPoin.setText(t.getLocalizedMessage());
                    }

                });
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {

            }
        });
        alertDialog.show();


    }


    RewardsModel rewardsModel;
    ApiRedeem apiRedeem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rewards);
        ButterKnife.bind(this);


        apiRedeem = ServiceGenerator.createService(ApiRedeem.class);

        Bundle b = getIntent().getExtras();

        rewardsModel = (RewardsModel) b.getSerializable("rewardsModel");

        textNamaRewards.setText(rewardsModel.getNama_rewards());
        textDeskripsiRewards.setText(rewardsModel.getDeskripsi_rewards());
        textPoin.setText(rewardsModel.getPoin());
        textSisa.setText(rewardsModel.getSisa());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
