package plbtw.epidemicdata.activitiy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import plbtw.epidemicdata.R;
import plbtw.epidemicdata.api.API;
import plbtw.epidemicdata.api.ServiceGenerator;
import plbtw.epidemicdata.callbacks.OnActionbarListener;
import plbtw.epidemicdata.model.DeletePenyakitResponse;
import plbtw.epidemicdata.model.DiseaseModel;
import plbtw.epidemicdata.model.PutPenyakitBody;
import plbtw.epidemicdata.model.PutPenyakitResponse;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class EditPenyakitActivity extends BaseActivity {

    private Toolbar toolbar;

    private TextView detailTxtUserName;
    private EditText detailTxtDiseaseName;
    private EditText detailTxtType;
    private EditText detailTxtNumber;
    private EditText detailTxtDescription;
    private EditText detailTxtSympton;
    private EditText detailTxtSuggest;

    private Button buttonSimpan;



    private RelativeLayout btnGetMap;

    public DiseaseModel diseaseData;

    private API api;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = getIntent();
        Bundle b = i.getExtras();


        diseaseData = (DiseaseModel) i.getSerializableExtra("diseaseModel");

        api = ServiceGenerator.createService(API.class);
    }

    @Override
    public void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


         detailTxtUserName = (TextView) findViewById(R.id.disease_detail_uploader_name);
         detailTxtDiseaseName= (EditText) findViewById(R.id.disease_detail_name);
         detailTxtType = (EditText) findViewById(R.id.disease_detail_type);
         detailTxtNumber = (EditText) findViewById(R.id.disease_detail_number);
         detailTxtDescription = (EditText) findViewById(R.id.disease_detail_desc);
         detailTxtSympton = (EditText) findViewById(R.id.disease_detail_gejala);
         detailTxtSuggest = (EditText) findViewById(R.id.disease_detail_saran);



         btnGetMap = (RelativeLayout) findViewById(R.id.get_map_btn);
        buttonSimpan = (Button) findViewById(R.id.button_simpan);
        buttonSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPenyakit(v);
            }
        });



    }

    private void editPenyakit(View v)
    {

        final ProgressDialog dialog = new ProgressDialog(v.getContext());
        dialog.setMessage("Please wait...");
        dialog.show();
        Call<PutPenyakitResponse> call = null;
        PutPenyakitBody putPenyakitBody = new PutPenyakitBody();


        putPenyakitBody.setDeskripsi(detailTxtDescription.getText().toString());
        putPenyakitBody.setGejala(detailTxtSympton.getText().toString());
        putPenyakitBody.setJumlah_penderita(detailTxtNumber.getText().toString());
        putPenyakitBody.setNama_penyakit(detailTxtDiseaseName.getText().toString());
        putPenyakitBody.setSaran_penanganan(detailTxtSuggest.getText().toString());

        call = api.editPenyakit(diseaseData.getId_penyakit(), putPenyakitBody);
        call.enqueue(new Callback<PutPenyakitResponse>() {
            @Override
            public void onResponse(Response<PutPenyakitResponse> response, Retrofit retrofit) {
                dialog.dismiss();
                if (2 == response.code() / 100) {
                    finish();
                } else {
                    Toast.makeText(EditPenyakitActivity.this, "Error edit penyakit", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(EditPenyakitActivity.this, "Failed edit penyakit", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void setUICallbacks() {
        setActionbarListener(new OnActionbarListener() {
            @Override
            public void onLeftIconClick() {
                onBackPressed();
            }

            @Override
            public void onRightIconClick() {

            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_edit_penyakit;
    }

    @Override
    public void updateUI() {
        fetchData();

    }

    public void fetchData(){

        detailTxtUserName.setText(diseaseData.getNama());
        detailTxtDiseaseName.setText(diseaseData.getNama_penyakit());
        detailTxtType.setText(diseaseData.getNama_tipe_penyakit());
        detailTxtNumber.setText(diseaseData.getJumlah_penderita());
        detailTxtDescription.setText(diseaseData.getDeskripsi());
        detailTxtSympton.setText(diseaseData.getGejala());
        detailTxtSuggest.setText(diseaseData.getSaran_penanganan());
    }
}
