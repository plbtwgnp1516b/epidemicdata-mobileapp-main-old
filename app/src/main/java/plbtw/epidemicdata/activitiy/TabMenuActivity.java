package plbtw.epidemicdata.activitiy;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.widget.TextView;



import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import plbtw.epidemicdata.fragment.ChallangeFragment;
import plbtw.epidemicdata.fragment.LokasiFragment;
import plbtw.epidemicdata.fragment.RedeemFragment;
import plbtw.epidemicdata.fragment.RewardsFragment;
import plbtw.epidemicdata.R;


public class TabMenuActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ViewPagerAdapter adapter;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_menu);
        ButterKnife.bind(this);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        Bundle b = getIntent().getExtras();
        if(b != null)
        {
//            if(b.getString("submit").equalsIgnoreCase("yes"))
//            {
//                Snackbar.make(coordinatorLayout, "Article Succesfully Submited",
//                        Snackbar.LENGTH_LONG).show();
//            }
        }
    }

    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabOne.setText("Challange");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_articles_white, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabTwo.setText("Rewards");

        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_trending_white, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabThree.setText("Redeem");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_push_notif_white, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabFour.setText("Lokasi");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_push_notif_white, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabFour);

    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ChallangeFragment(), "Challange");
        adapter.addFrag(new RewardsFragment(), "Rewards");
        adapter.addFrag(new RedeemFragment(), "Redeem");
        adapter.addFrag(new LokasiFragment(), "Lokasi");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}
