package plbtw.epidemicdata.activitiy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
<<<<<<< HEAD:app/src/main/java/plbtw/epidemicdata/activities/ExploreDetailActivity.java
import android.widget.GridView;
=======
import android.view.View;
>>>>>>> 412043fb886c9e0bf2e2fda6c91abd98241740bc:app/src/main/java/plbtw/epidemicdata/activitiy/ExploreDetailActivity.java
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import plbtw.epidemicdata.R;
<<<<<<< HEAD:app/src/main/java/plbtw/epidemicdata/activities/ExploreDetailActivity.java
import plbtw.epidemicdata.adapter.DiseaseImageAdapter;
=======
import plbtw.epidemicdata.api.API;
import plbtw.epidemicdata.api.ServiceGenerator;
>>>>>>> 412043fb886c9e0bf2e2fda6c91abd98241740bc:app/src/main/java/plbtw/epidemicdata/activitiy/ExploreDetailActivity.java
import plbtw.epidemicdata.callbacks.OnActionbarListener;
import plbtw.epidemicdata.model.DeletePenyakitResponse;
import plbtw.epidemicdata.model.DiseaseModel;
<<<<<<< HEAD:app/src/main/java/plbtw/epidemicdata/activities/ExploreDetailActivity.java
import plbtw.epidemicdata.utils.FunctionUtil;
=======
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
>>>>>>> 412043fb886c9e0bf2e2fda6c91abd98241740bc:app/src/main/java/plbtw/epidemicdata/activitiy/ExploreDetailActivity.java


public class ExploreDetailActivity extends BaseActivity {

    private Toolbar toolbar;

    private TextView detailTxtUserName;
    private TextView detailTxtDiseaseName;
    private TextView detailTxtType;
    private TextView detailTxtNumber;
    private TextView detailTxtDescription;
    private TextView detailTxtSympton;
    private TextView detailTxtSuggest;

    private GridView pictureList;

    private TextView btnDelete;

    private RelativeLayout btnGetMap;

    private DiseaseImageAdapter adapter;

    private ArrayList<String> diseaseImageUrls;

    public DiseaseModel diseaseData;

    private API api;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new DiseaseImageAdapter(this);
        diseaseImageUrls = new ArrayList<>();

        Intent i = getIntent();
        Bundle b = i.getExtras();


        diseaseData = (DiseaseModel) i.getSerializableExtra("disease");

        api = ServiceGenerator.createService(API.class);
    }

    @Override
    public void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


         detailTxtUserName = (TextView) findViewById(R.id.disease_detail_uploader_name);
         detailTxtDiseaseName= (TextView) findViewById(R.id.disease_detail_name);
         detailTxtType = (TextView) findViewById(R.id.disease_detail_type);
         detailTxtNumber = (TextView) findViewById(R.id.disease_detail_number);
         detailTxtDescription = (TextView) findViewById(R.id.disease_detail_desc);
         detailTxtSympton = (TextView) findViewById(R.id.disease_detail_gejala);
         detailTxtSuggest = (TextView) findViewById(R.id.disease_detail_saran);

         pictureList = (GridView) findViewById(R.id.disease_image_list);


         btnDelete = (TextView) findViewById(R.id.btn_delete);
         btnGetMap = (RelativeLayout) findViewById(R.id.get_map_btn);

<<<<<<< HEAD:app/src/main/java/plbtw/epidemicdata/activities/ExploreDetailActivity.java
        int deviceWidth = FunctionUtil.getDeviceSize(this).x;
        RelativeLayout.LayoutParams gridViewParams = (RelativeLayout.LayoutParams)pictureList.getLayoutParams();
        gridViewParams.width = (deviceWidth / 3) * 2;
        gridViewParams.height = deviceWidth;
        gridViewParams.topMargin = -(gridViewParams.width / 4);
        gridViewParams.bottomMargin = gridViewParams.topMargin;
        gridViewParams.leftMargin = -(gridViewParams.topMargin);
        pictureList.setLayoutParams(gridViewParams);

=======
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deletePenyakit(v);
            }
        });

    }

    private void deletePenyakit(View v)
    {
        final ProgressDialog dialog = new ProgressDialog(v.getContext());
        dialog.setMessage("Please wait...");
        dialog.show();

        Call<DeletePenyakitResponse> call = null;
        call = api.deletePenyakit(diseaseData.getId_penyakit());
        call.enqueue(new Callback<DeletePenyakitResponse>() {
            @Override
            public void onResponse(Response<DeletePenyakitResponse> response, Retrofit retrofit) {
                dialog.dismiss();
                if (2 == response.code() / 100) {
                    finish();
                } else {
                    Toast.makeText(ExploreDetailActivity.this, "Error delete penyakit", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(ExploreDetailActivity.this, "Failed delete penyakit", Toast.LENGTH_SHORT).show();
            }
        });
>>>>>>> 412043fb886c9e0bf2e2fda6c91abd98241740bc:app/src/main/java/plbtw/epidemicdata/activitiy/ExploreDetailActivity.java
    }

    @Override
    public void setUICallbacks() {
        setActionbarListener(new OnActionbarListener() {
            @Override
            public void onLeftIconClick() {
                onBackPressed();
            }

            @Override
            public void onRightIconClick() {

            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_explore_detail;
    }

    @Override
    public void updateUI() {
        fetchData();
        adapter.setData(diseaseImageUrls);
        pictureList.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }

    public void fetchData(){

        detailTxtUserName.setText(diseaseData.getNama());
        detailTxtDiseaseName.setText(diseaseData.getNama_penyakit());
        detailTxtType.setText(diseaseData.getNama_tipe_penyakit());
        detailTxtNumber.setText(diseaseData.getJumlah_penderita());
        detailTxtDescription.setText(diseaseData.getDeskripsi());
        detailTxtSympton.setText(diseaseData.getGejala());
        detailTxtSuggest.setText(diseaseData.getSaran_penanganan());

        for (int i = 0; i < diseaseData.getMultimedia().size(); i++) {
            diseaseImageUrls.add(diseaseData.getMultimedia().get(i).getUrl());
        }

        adapter.notifyDataSetChanged();
    }
}
