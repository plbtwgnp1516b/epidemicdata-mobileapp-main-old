package plbtw.epidemicdata.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import plbtw.epidemicdata.activitiy.DetailRewardsActivity;
import plbtw.epidemicdata.activitiy.EditPenyakitActivity;
import plbtw.epidemicdata.model.DiseaseModel;
import plbtw.epidemicdata.R;

public class ExploreAdapter extends RecyclerView.Adapter {
    private Context context;

    private LayoutInflater inflater;

    private ArrayList<DiseaseModel> diseaseModelList;
    private ExploreAdapterListener listener;

    public void setContext(Context context) {
        this.context = context;
    }

    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public ExploreAdapter(ArrayList<DiseaseModel> diseaseModelList, ExploreAdapterListener listener) {
        this.diseaseModelList = diseaseModelList;
        this.listener = listener;

    }

    public interface ExploreAdapterListener {
        void onItemClick(int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_item, parent, false);
        ImageView imageDisease = (ImageView)v.findViewById(R.id.imageView3);
        TextView textNamaPenyakit = (TextView)v.findViewById(R.id.txt_namaPenyakit);
        TextView textTipePenyakit = (TextView)v.findViewById(R.id.txt_tipePenyakit);
        TextView textLokasi = (TextView)v.findViewById(R.id.txt_lokasi);
        TextView textNama = (TextView)v.findViewById(R.id.txt_namaUser);
        View item = v.findViewById(R.id.explore_item_item);

        return new ViewHolder(v, imageDisease, textNamaPenyakit, textTipePenyakit, textLokasi, textNama, item);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {



        final ViewHolder challangeViewHolder = (ViewHolder) holder;

        final DiseaseModel diseaseModel = diseaseModelList.get(position);

        challangeViewHolder.textNama.setText(diseaseModel.getNama());

        challangeViewHolder.textNamaPenyakit.setText(diseaseModel.getNama_penyakit());
        challangeViewHolder.textTipePenyakit.setText(diseaseModel.getNama_tipe_penyakit());

        challangeViewHolder.textLokasi.setText(diseaseModel.getNama_desa());

        challangeViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(position);
            }
        });

        challangeViewHolder.item.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                DiseaseModel diseaseModel = diseaseModelList.get(position);
                ShowOptionMenu(diseaseModel, v);
                return true;
            }
        });


    }


    private void ShowOptionMenu(final DiseaseModel diseaseModel, final View view) {
        final String[] items;
        final Integer[] icons;
        items = new String[]{"Edit Penyakit"};
        icons = new Integer[]{R.drawable.ic_post};

        ListAdapter listAdapter = new ArrayAdapterWithIcon(view.getContext(), items, icons);
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setAdapter(listAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int item) {
                if (items[item].equals("Edit Penyakit")) {
                    showEditPenyakit(diseaseModel, view);
                }
            }
        });
        builder.show();
    }

    private void showEditPenyakit(DiseaseModel diseaseModel, View view)
    {
        Intent intent = new Intent(view.getContext(), EditPenyakitActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("diseaseModel", diseaseModel);
        intent.putExtras(b);
        view.getContext().startActivity(intent);
    }


    @Override
    public int getItemCount() {
        return diseaseModelList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageDisease;
        public TextView textNamaPenyakit;
        public TextView textTipePenyakit;
        public TextView textLokasi;
        public TextView textNama;
        public View item;


        public ViewHolder(View itemView, ImageView imageDisease, TextView textNamaPenyakit,
                          TextView textTipePenyakit, TextView textLokasi, TextView textNama, View item) {
            super(itemView);
            this.imageDisease = imageDisease;
            this.textNamaPenyakit = textNamaPenyakit;
            this.textTipePenyakit = textTipePenyakit;
            this.textLokasi = textLokasi;
            this.textNama = textNama;
            this.item = item;
        }
    }



}
