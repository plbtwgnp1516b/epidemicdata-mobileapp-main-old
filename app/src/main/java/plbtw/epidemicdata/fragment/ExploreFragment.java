package plbtw.epidemicdata.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import plbtw.epidemicdata.activitiy.ExploreDetailActivity;
import plbtw.epidemicdata.activitiy.MainActivity;
import plbtw.epidemicdata.adapter.ExploreAdapter;
import plbtw.epidemicdata.model.DiseaseModel;
import plbtw.epidemicdata.model.DiseaseResponse;
import plbtw.epidemicdata.R;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DedeEko on 5/18/2016.
 */
public class ExploreFragment extends BaseFragment implements ExploreAdapter.ExploreAdapterListener{

    MainActivity activity;

    private RecyclerView recyclerView;

    private ArrayList<DiseaseModel> listPenyakit;

    private ExploreAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_explore,
                container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.explore_list);
        recyclerView.setHasFixedSize(false);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);


        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = (MainActivity) getActivity();

        listPenyakit = new ArrayList<>();

        adapter = new ExploreAdapter(listPenyakit, this);


    }

    @Override
    public void initView(View view) {

    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

        setupActionBar();

    }

    @Override
    public String getPageTitle() {
        return "Explore";
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_explore;
    }

    private void setupActionBar() {
        MainActivity mainActivity = (MainActivity)getActivity();
        mainActivity.setDefaultActionbarIcon();
        mainActivity.setLeftIcon(0);
    }

    public void fetchData(){
        Call<DiseaseResponse> call = null;
        call = activity.api.getDiseaseList("1","1000");
        call.enqueue(new Callback<DiseaseResponse>() {
            @Override
            public void onResponse(Response<DiseaseResponse> response, Retrofit retrofit) {

                if (2 == response.code() / 100) {
                    final DiseaseResponse diseaseResponse = response.body();

                    listPenyakit = diseaseResponse.getListDisease();

                    adapter = new ExploreAdapter(listPenyakit, ExploreFragment.this );

                    recyclerView.setAdapter(adapter);

                } else {
                    Toast.makeText(activity.getApplicationContext(), "Cannot fetching data.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    public void run() {
        setupActionBar();
        fetchData();
    }

    @Override
    public void onItemClick(int position) {
        DiseaseModel item = listPenyakit.get(position);

        Intent i = new Intent(getActivity(), ExploreDetailActivity.class );
        i.putExtra("disease", item);
        startActivity(i);
    }
}
