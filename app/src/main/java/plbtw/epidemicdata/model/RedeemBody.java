package plbtw.epidemicdata.model;

public class RedeemBody {

    private String id_redeem;
    private String redeem_key;
    private String id_user_app;
    private String id_rewards;


    public String getId_redeem() {
        return id_redeem;
    }

    public void setId_redeem(String id_redeem) {
        this.id_redeem = id_redeem;
    }

    public String getRedeem_key() {
        return redeem_key;
    }

    public void setRedeem_key(String redeem_key) {
        this.redeem_key = redeem_key;
    }

    public String getId_user_app() {
        return id_user_app;
    }

    public void setId_user_app(String id_user_app) {
        this.id_user_app = id_user_app;
    }

    public String getId_rewards() {
        return id_rewards;
    }

    public void setId_rewards(String id_rewards) {
        this.id_rewards = id_rewards;
    }
}
