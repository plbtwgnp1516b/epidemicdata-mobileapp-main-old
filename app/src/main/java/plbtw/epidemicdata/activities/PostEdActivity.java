package plbtw.epidemicdata.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import plbtw.epidemicdata.R;
import plbtw.epidemicdata.api.API;
import plbtw.epidemicdata.callbacks.OnActionbarListener;
import plbtw.epidemicdata.model.DiseaseBody;
import plbtw.epidemicdata.model.DiseasePostResponse;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DedeEko on 6/1/2016.
 */
public class PostEdActivity extends BaseActivity {

    private Toolbar toolbar;

    public MainActivity mainActivity;

    private EditText namaPenyakit;
    private EditText jumlahPenderita;
    private EditText deskripsi;
    private EditText gejala;
    private EditText saran;

    private TextView tipePenyakit;
    private TextView Lokasi;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setRightIcon(0);
        setActionBarTitle("Post ed!");
        actionRight.setBackgroundColor(getResources().getColor(R.color.actionbar_dark_color));

    }

    @Override
    public void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        namaPenyakit = (EditText) findViewById(R.id.form_item_name);
        jumlahPenderita= (EditText) findViewById(R.id.form_item_number);
        deskripsi= (EditText) findViewById(R.id.form_item_desc);
        gejala= (EditText) findViewById(R.id.form_item_gejala);
        saran= (EditText) findViewById(R.id.form_item_saran);

        tipePenyakit= (TextView) findViewById(R.id.form_item_type);
        Lokasi= (TextView) findViewById(R.id.form_item_location);
    }

    @Override
    public void setUICallbacks() {
        setActionbarListener(new OnActionbarListener() {
            @Override
            public void onLeftIconClick() {
                onBackPressed();

            }

            @Override
            public void onRightIconClick() {
                postPenyakit();
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_post_ed;
    }

    @Override
    public void updateUI() {

    }

    public void postPenyakit()
    {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.show();
        Call<DiseasePostResponse> call = null;
        DiseaseBody diseaseBody = new DiseaseBody();
        diseaseBody.setId_user_app("2");
        diseaseBody.setNama_penyakit(namaPenyakit.getText().toString());
        diseaseBody.setId_tipe_penyakit("1");
        diseaseBody.setId_desa("1");
        diseaseBody.setJumlah_penderita(jumlahPenderita.getText().toString());
        diseaseBody.setDeskripsi(deskripsi.getText().toString());
        diseaseBody.setGejala(gejala.getText().toString());
        diseaseBody.setSaran_penanganan(saran.getText().toString());

        call = api.postDisease(diseaseBody);
        call.enqueue(new Callback<DiseasePostResponse>() {
            @Override
            public void onResponse(Response<DiseasePostResponse> response, Retrofit retrofit) {
                dialog.dismiss();
                if (201 == response.code()) {

                    finish();

                } else {
                    Toast.makeText(PostEdActivity.this, "Error post redeem", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(PostEdActivity.this, "Failed post redeem", Toast.LENGTH_SHORT).show();
            }

        });
    }

}
